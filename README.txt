Automatically display advertisements from the Ad Bard Network on your 
Drupal-powered website.

The Ad Bard Network is dedicated to fostering a friendly and useful advertising 
community, built from and focused on Free and Open Source Software.  Your 
website must be somehow related to Free and Open Source Software to join the Ad
Bard Network.  For more information, visit http://adbard.net/.

CONFIGURATION:
--------------
0) Install and enable the adbard module.

1) Visit "Administer" => "Site configuration" => "Ad Bard Network" and follow
   the instructions on that page to find and enter your Ad Bard Key.

2) Visit "Administer" => "Site building" => "Blocks" and enable any of the
   blocks whose names start with "Ad Bard Network:".  For example, to display
   175x125 ad image blocks on your website enable the block named "Ad Bard
   Network: 175x125 Ad Image Blocks".

   As with any drupal block, you can control where it appears by setting the
   "Region" and a "Weight" for the block.  If you click "configure" next to
   the block, you can control on which pages the advertisements appear, and
   to users in which Drupal roles.  You can also make it possible for logged
   in users on your site to disable the advertisements.
